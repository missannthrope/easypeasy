---
title: "EasyPeasy"
author: "Hackauthor²"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output:
  bookdown::gitbook:
    config:
      fontsettings:
        theme: night
        family: sans
        size: 2
      edit : https://gitlab.com/snuggy/easypeasy/%s
    includes:
      in_header: header.html
    fig_caption: false
  pdf_document:
    includes:
      in_header: preamble.tex
      toc: true
      number_sections: true
      fig_caption: false
      latex_engine: xelatex

documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
url: 'https\://easypeasymethod.org/'
cover-image: "images/easypeasy.jpg"
apple-touch-icon: "touch-icon.png"
apple-touch-icon-size: 120
favicon: "favicon.ico"
description: "Painlessly quit pornography immediately, without willpower or any sense of deprivation or sacrifice."
---
